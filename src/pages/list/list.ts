import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
})
export class ListPage {
  items: string[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidEnter() {
    this.items = [];
    let localNotes = JSON.parse(localStorage.getItem('notes'));
    if (localNotes) {
      localNotes.forEach( element => {
        this.items.push(element);
      });
    }
  }

  addNew() {
    this.navCtrl.push( 'NotePage', { isNew: true, index: this.items.length });
  }

  goToNote( index: number ) {
    this.navCtrl.push( 'NotePage', { isNew: false, index: index });
  }

}
