## Installation

* install Node.js
* `npm install -g ionic cordova`
* `npm i`
* `ionic cordova build ios`
* `ionic cordova run ios`

building for IOS:

* install XCode
* `sudo xcode-select -s /Applications/Xcode-Beta.app/Contents/Developer`
* `ionic cordova build ios`
* run XCode and select directory: [project name]/platforms/ios
* select device and click Run
