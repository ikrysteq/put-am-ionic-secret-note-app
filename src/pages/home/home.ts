import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  resultCheck: any;
  private pass: string;

  constructor(
    public navCtrl: NavController,
    public fingerprint: FingerprintAIO,
    private alertCtrl: AlertController ) {
  }

  ionViewDidLoad() {
    setTimeout(() => {
      this.pass = localStorage.getItem('pass');
      if(!!this.pass) {
        this.check();
      } else {
        this.setPassword();
      }
    }, 500);
  }

  check() {
    this.fingerprint.isAvailable().then( result => {
      this.resultCheck = result;
      this.showFP();
    } ).catch( err => {
      console.log( err );
      this.resultCheck = err;
      this.presentPrompt();
    } );
  }

  showFP() {
    this.fingerprint.show( {
     clientId: "Zaloguj się do aplikacji za pomocą odcisku palca."
    } ).then( result => {
      console.log( result );
      this.navCtrl.push('ListPage');
    } ).catch( err => {
      console.log(err);
    } );
  }

  presentPrompt() {
    let alert = this.alertCtrl.create({
      title: 'Podaj hasło',
      inputs: [
        {
          name: 'password',
          placeholder: 'Password',
          type: 'password'
        }
      ],
      buttons: [
        {
          text: 'Anuluj',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Zatwierdź',
          handler: data => {
            this.pass = localStorage.getItem('pass');
            if (data.password === this.pass) {
              // logged in!
              this.navCtrl.push('ListPage');
            } else {
              // invalid login
              return false;
            }
          }
        }
      ]
    });
    alert.present();
  }

  setPassword() {
    let alert = this.alertCtrl.create({
      title: 'Ustaw hasło',
      inputs: [
        {
          name: 'password',
          placeholder: 'Password',
          type: 'password'
        }
      ],
      buttons: [
        {
          text: 'Zatwierdź',
          handler: data => {
            if (data.password) {
              // password set!
              localStorage.setItem('pass', data.password);
            } else {
              // failure
              this.setPassword();
            }
          }
        }
      ]
    });
    alert.present();
  }
}
