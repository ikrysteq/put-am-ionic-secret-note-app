import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the NotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-note',
  templateUrl: 'note.html',
})
export class NotePage {
  title: string = '';
  description: string = '';
  isNew: boolean;
  index: number;
  localNotes: string[];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidEnter() {
    this.localNotes = JSON.parse(localStorage.getItem('notes'));
    this.index = this.navParams.get('index');
    this.isNew = this.navParams.get('isNew');
    if (this.isNew) {
      this.title = 'Nowa notatka';
    } else {
      this.title = this.localNotes[ this.index ];
      this.description = this.localNotes[ this.index ];
;    }
  }

  descriptionChanged( currentValue: string ) {
    this.title = currentValue.substring(0, 13);
    currentValue.length > 13 ? this.title = this.title + '...' : {};
  }

  save() {
    if ( this.localNotes ) {
      this.localNotes[ this.index ] = this.description;
    } else {
      this.localNotes = [];
      this.localNotes.push(this.description);
    }
    localStorage.setItem('notes', JSON.stringify(this.localNotes));
    this.navCtrl.pop();
  }

}
